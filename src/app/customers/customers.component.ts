import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { CustomersService } from '../customers.service';
import { Customer } from '../interfaces/customer';



@Component({
  selector: 'customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  userId;

  students:Customer[];
  students$;
  addItemFormOpen;
  rowToEdit:number = -1; 
  itemToEdit = {math:null,  psico:null, pay:null};

  completed: false;

  deleteStudent(index){
    let id = this.students[index].id;
    console.log(id);
    this.customersService.deleteStudent(this.userId, id);
   }
  

  
  
  // moveToEditState(index){
  //   this.itemToEdit.name = this.items[index].math;
  //   this.itemToEdit.price = this.items[index].psico;
  //   this.itemToEdit.inStock = this.items[index].pay;
  //   this.rowToEdit = index; 
  // }

  // updateItem(){
  //   let id = this.items[this.rowToEdit].id;
  //   this.customersService.updateItem(this.userId,id, this.itemToEdit.name,this.itemToEdit.price,this.itemToEdit.inStock);
  //   this.rowToEdit = null;
  // }
  // deleteSelected(){
  //   let ind;
  //   console.log(this.list);
  //   for( ind in this.list){
  //     if(this.list[ind]==true){
  //       this.customersService.deleteItems(this.userId,this.items[ind].id)
  //     }
  //   }
  // }
  // change value acording to checkBox

  /**updateFromCheck(index){
    if(this.customers[index].result=='Will pay'){
      this.customers[index].result='Will default';
    }else{
      this.customers[index].result='Will pay';
    }
    this.customers[index].saved = true; 
    this.customersService.updateRsult(this.userId,this.customers[index].id,this.customers[index].result);

  }*/
 
  // checkSelected(i){
  //  this.isChecked[i]=!this.isChecked[i]
  //  if(this.isChecked[i]==true){
  //    this.list[i]=true;
  //  }else{
  //    this.list[i]=false;
  //  }
  //  console.log(this.list);
  // }
   

  // updateResult(index){
  //   this.items[index].saved = true; 
  //   this.customersService.updateResult(this.userId,this.items[index].id,this.items[index].result);
  // }

  // predict(index){
  //   this.items[index].result = 'Will difault';
  //   this.predictService.predict(this.items[index].name, this.items[index].price, this.items[index].inStock).subscribe(
  //     res => {
  //       console.log(res);
  //       if(res > 0.5){
  //         console.log('bigger');
  //         var result = 'Will pay';
  //       } else {
  //         console.log('smaller');
  //         var result = 'Will not pay'
  //       }
  //       console.log(result);

  //       this.items[index].result = result}
  //   );  
  // }

  displayedColumns: string[] = ['email','math', 'psico', 'pay','prediction','Delete', 'date'];
 
  constructor(private customersService:CustomersService,
    private authService:AuthService
     ) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
          this.userId = user.uid;
         
          this.students$ = this.customersService.getStudent(this.userId);
          this.students$.subscribe(
            docs => {         
              this.students = [];
              var i = 0;
              for (let document of docs) {
 
                const student:Customer = document.payload.doc.data();
                student.id = document.payload.doc.id;
                console.log('here')
                   this.students.push(student);
              }                        
            }
          )
      })
      
  }   
}

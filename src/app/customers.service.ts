import { AngularFirestoreModule, AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  studentsCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection=this.db.collection('users');

  
  getStudent(userId): Observable<any[]> {
    this.studentsCollection = this.db.collection(`users/${userId}/students`, 
       ref => ref.orderBy('date', 'desc'));
    return this.studentsCollection.snapshotChanges();
  } 


  deleteStudent(userId:string, id:string){
    this.db.doc(`users/${userId}/students/${id}`).delete();
     
  }
  
  addStudent(userId:string, math:number, psico:number, pay:boolean, prediction:string, email, date){
    const student = {
      math:math,
      psico:psico,
      pay:pay,
      prediction:prediction,
      email:email,
      date:date
    };
    this.userCollection.doc(userId).collection('students').add(student);
  }

  public updateItem(userId:string, id:string, name:string, price:number, inStock:number){
    this.db.doc(`users/${userId}/items/${id}`).update(
      {
        name:name,
        price:price,
        inStock:inStock,
      })}
      public updateLikes(userId:string, id:string, likes:number){
        this.db.doc(`users/${userId}/posts/${id}`).update(
          {
            likes:likes
          })}
      

  
  updateResult(userId:string, id:string,result:string){
    this.db.doc(`users/${userId}/items/${id}`).update(
      {
        result:result
      })
    }
  

  addItem(userId:string,name:string,price:number, inStock:number){
    const item = {name:name, price:price,inStock:inStock }; 
    this.userCollection.doc(userId).collection('items').add(item);
  }
  

  constructor(private db:AngularFirestore) { }
}








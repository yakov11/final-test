import { HttpClient } from '@angular/common/http';
import {  Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';






@Injectable({
  providedIn: 'root'
})
export class PredictService {
  private url =  "https://pm3syupnh3.execute-api.us-east-1.amazonaws.com/beta";
  
  text;
  comments$;
  classifyRes;
  uuu;

  predictLeave(math:number, psico:number, pay:boolean):Observable<any>{
    let json = {
          "math": math,
          "psico": psico,
          "pay":pay
        }
    let body  = JSON.stringify(json);
    console.log(body);
    console.log("in predict");
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log(res);
        return res;       
      })
    );      
  }


  // getPosts():Observable<Post>{
  //   return this.http.get<Post>(this.URL); 
  // }
  

  // getComment(id):Observable<Post>{
  //   this.uuu = (`${this.URL}${id}/comments`);
  //   return this.http.get<Post>(this.uuu)
  // }

  

  // sendComments(){
  //   console.log(this.uuu);
  //   return this.http.get<Post>(this.uuu)
    
  // }

  constructor(private http:HttpClient, public router:Router) { }
  
}

import { PredictService } from './../predict.service';
import { Router, RouterModule } from '@angular/router';
import { CustomersService } from './../customers.service';
import { AuthService } from './../auth.service';
import { Component, OnInit, Input,  Output, EventEmitter } from '@angular/core';



@Component({
  selector: 'customerform',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})
export class CustomerFormComponent implements OnInit {

math: number;
psico: number;
pay: boolean= false;
prediction = "not yet";
statuses = ['nosher', 'loNosher'];
press:boolean=false;
userId;
email;

add(){
  this.customersService.addStudent(this.userId, this.math, this.psico, this.pay, this.prediction, this.email)
  this.router.navigate(['/costumers']);

}

predict(){
  this.press = true;
    this.predictService.predictLeave(this.math, this.psico,  this.pay).subscribe(
         res => {
           console.log(res);
          if(res > 0.5){
            console.log ('bigger')
             this.prediction = 'lo nosher';
          } else {
            console.log ('smaller')
            this.prediction = 'nosher'
          }})}
         
   
   
  




cancle(){
  this.press = false;

}

  constructor(public authService:AuthService, public customersService:CustomersService, public predictService:PredictService, public router:Router) { }


  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
          this.userId = user.uid;
          this.email = user.email;
  })


}}
